# Return all Orders
orders = Order.objects.all().count()
products = Product.objects.all().order_by('-id')

# Return Pending orders
pending_order = Order.objects.filter(status="Pendiente").count()

# Return paid orders
paid_order = Order.objects.filter(status="Pagado").count()

# Return delivered orders
delivered_order = Order.objects.filter(status="Entregado").count()


