from django.apps import AppConfig


class SalescrudConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'salescrud'
