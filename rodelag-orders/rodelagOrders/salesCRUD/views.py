from django.shortcuts import render, redirect
from http.client import HTTPResponse
from matplotlib.style import context
# Create your views here.
from .models import *
from .form import OrderForm


def crud(request):
    orders = Order.objects.all().order_by('-id')

    total_orders = Order.objects.all().count()
    pending_order = Order.objects.filter(status="Pendiente").count()
    paid_order = Order.objects.filter(status="Pagado").count()
    delivered_order = Order.objects.filter(status="Entregado").count()

    context = {'orders':orders,'total_orders':total_orders,'pending_order':pending_order,
    'paid_order':paid_order,'delivered_order':delivered_order }

    return render(request, 'salescrud/dashboard.html', context)

def createOrder(request):
    
    form = OrderForm()
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
            
    context = {'form':form}
    return render(request, 'salescrud/order_form.html', context)

def updateOrder(request, pk):

    order = Order.objects.get(id=pk)
    form = OrderForm(instance=order)
    
    if request.method == 'POST':
        form = OrderForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            return redirect('/')

    context = {'form':form}
    return render(request, 'salescrud/order_form.html', context)

def deleteOrder(request, pk):
    order = Order.objects.get(id=pk)
    if request.method == "POST":
        order.delete()
        return redirect('/')

    context = {'item':order}
    return render(request, 'salescrud/delete.html', context)