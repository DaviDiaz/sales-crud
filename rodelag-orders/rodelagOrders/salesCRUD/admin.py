from django.contrib import admin

# Register your models here.
from .models import Client, Order, Product, Tag

admin.site.register(Client)
admin.site.register(Tag)
admin.site.register(Product)
admin.site.register(Order)

