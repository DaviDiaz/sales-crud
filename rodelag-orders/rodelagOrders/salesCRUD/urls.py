from django.urls import path
from . import views



urlpatterns = [
    path('', views.crud, name="crud"),
    path('create_order/', views.createOrder, name="create_order"),
    path('update_order/<str:pk>/', views.updateOrder, name="update_order"),
    path('delete_order/<str:pk>/', views.deleteOrder, name="delete_order")

]
